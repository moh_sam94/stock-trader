# stock-trader

## Demo
[Click here](https://stock-trader-app-alpha.herokuapp.com/) to view a deployed version of the app.

# Overview

-   This project was implemented from [Udemy Vuejs Course](https://www.udemy.com/share/101WzMB0obcFdWR3Q=/)
-   It is an app where the user can buy and sell stocks.
-   The user can view his stocks, Save and load his status.
-   By Clicking End Day the stock prices will randomly change.

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your unit tests

```
npm run test
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

import { createLocalVue, mount } from '@vue/test-utils';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';

import { routes } from '../../routes';
import App from '../../App.vue';
import stocks from '../../store/modules/stocks.js';
import portfolio from '../../store/modules/portfolio.js';
jest.mock('vue-resource');

VueResource.mockImplementation(() => {
    return {
        get: () => {
            Promise.resolve({
                data: {
                    funds: 12711,
                    stockPortfolio: [
                        { id: 1, name: 'BMW', price: 50, quantity: 100 },
                    ],
                    stocks: [
                        { id: 1, name: 'BMW', price: 110 },
                        { id: 2, name: 'Google', price: 200 },
                        { id: 3, name: 'Apple', price: 250 },
                        { id: 4, name: 'Twitter', price: 8 },
                    ],
                },
            });
        },
    };
});
describe('App.vue', () => {
    const localVue = createLocalVue();
    localVue.use(VueRouter);
    localVue.use(Vuex);
    localVue.use(VueResource);
    localVue.filter('currency', value => {
        return '$' + value.toLocaleString();
        1;
    });
    const router = new VueRouter({
        mode: 'history',
        routes,
    });

    let actions = {
        initStocks: jest.fn(),
    };

    let store = new Vuex.Store({
        actions,
        modules: {
            portfolio,
            stocks,
        },
    });

    it('renders app component and its children', done => {
        const wrapper = mount(App, {
            router,
            store,
            localVue,
        });
        wrapper.vm.$nextTick(() => {
            expect(actions.initStocks).toHaveBeenCalled();
            expect(wrapper).toBeDefined();
            expect(wrapper).toMatchSnapshot();
            done();
        });
    });
});
